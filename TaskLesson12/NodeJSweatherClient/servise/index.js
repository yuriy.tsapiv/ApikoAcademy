const { GetGeocoding } = require('../get-geocoding');


const GetWeather = (city) => {
    city? GetGeocoding(city): GetWeather('Ternopil')
}

module.exports = {
    GetWeather
}