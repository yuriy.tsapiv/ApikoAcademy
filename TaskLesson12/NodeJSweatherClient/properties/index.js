require('dotenv').config();

const apiGeo = process.env.API_GEOCODING;
const keyApiGeo = process.env.API_KEY_GEOCODING;

const apiDarkSky = process.env.API_DARK_SKY;
const keyApiDarkSky = process.env.API_KEY_DARK_SKY;

module.exports ={
    apiGeo,
    keyApiGeo,
    apiDarkSky,
    keyApiDarkSky
}

