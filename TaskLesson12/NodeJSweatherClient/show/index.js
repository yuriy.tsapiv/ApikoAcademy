const Title = () => console.log(`
        *****************************************************
        *************** NodeJs Weather Client ***************
        *****************************************************`);

const Error = () => console.log(`
                            ERROR !
        Please check the name of the city and try again`);

const Weather = (city, data) => {
    const { currently, latitude, longitude, timezone } = data;
    const { summary, temperature, humidity, pressure, windSpeed } = currently;
    console.log(`
                           City: ${city}
Latitude: ${latitude} Longitude: ${longitude} TimeZone: ${timezone}
--------------------------------------------------------------------
                         Currently Weather

The weather in the ${city} are ${summary} and the temperature
reaches ${temperature}, the wind speed ${windSpeed}, atmospheric pressure ${pressure},
humidity ${humidity}.
 
`)
};

module.exports = {
    Title,
    Error,
    Weather,
}