const request = require('request');
const props = require('../properties');
const show = require('../show');

const { apiDarkSky, keyApiDarkSky } = props;

const GetDarkSky = (city ,latitude, longitude) => {

    const url = `${apiDarkSky}${keyApiDarkSky}/${latitude},${longitude}`;

    request(url,
        function (err, response, body) {
            if (err) {
                console.log('error', err);
            } else {
                let data = JSON.parse(body);
                show.Weather(city ,data);
            }
        })
}

module.exports = {
    GetDarkSky
}