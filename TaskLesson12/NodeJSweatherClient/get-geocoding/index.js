const request = require('request');
const props = require('../properties');
const { GetDarkSky } = require('../get-dark-sky');
const show = require('../show');

const { apiGeo, keyApiGeo } = props

const GetGeocoding = (city) => {

    const url = `${apiGeo}${city}&key=${keyApiGeo}`;

    request(url, function (err, response, body) {
        if (err) {
            console.log('error:', err);
        } else {
            try {
                let geo = JSON.parse(body)
                let results = geo.results;
                let latitude = results[0].geometry.location.lat;
                let longitude = results[0].geometry.location.lng;

                GetDarkSky(city, latitude, longitude);
            } catch (error) {
                show.Error();
            }

        }
    })
};

module.exports = {
    GetGeocoding
}