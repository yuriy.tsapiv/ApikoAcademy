const React = {
    createElement: (eleType, attrs, children = []) => {

        const ele = document.createElement(eleType);
        if (!ele)
            return false;

        for (var idx in attrs) {
            if ((idx == 'styles' || idx == 'style') && typeof attrs[idx] == 'object') {
                for (var prop in attrs[idx]) {
                    ele.style[prop] = attrs[idx][prop]
                }
            } else if (idx == 'html' || idx == 'textContent') {
                ele.innerHTML = attrs[idx];
            } else {
                ele.setAttribute(idx, attrs[idx]);
            }
        }

        if (typeof children == 'string') {
            ele.appendChild(document.createTextNode(children))
        }else {
            for (const cild of children) {
                (typeof cild == 'string') ? ele.appendChild(document.createTextNode(cild)) : ele.appendChild(cild);
            }
        }

        return ele;
    },
    render: (component, targrtEle) => {
        targrtEle.appendChild(component)
    },
}

const app =
    React.createElement('div', { style: { backgroundColor: 'red' } }, [
        React.createElement('span', undefined, 'Hello world'),
        React.createElement('br'),
        'This is just a text node',
        React.createElement('div', { textContent: 'Text content' }),
    ]);
React.render(
    app,
    document.getElementById("root"),
);
