import { compose, withStateHandlers, withHandlers, lifecycle, branch, renderComponent } from 'recompose';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { db } from '../../utils';

import AppLoader from '../Loaders/AppLoader';
import Component from './Component';
import store from "../../modules/store";

const mapStateToProps = state => ({
  user: state.user,
    // TODO: CODE FOR YOUR HOMEWORK HERE, done
    //answerSort: state.answerSort - if make all logic inside Component
});

const votesByAnswerId = (votes, answerId) => votes.filter(vote => vote.answerId === answerId);

const divideVotes = votes => {
  const positive = votes.filter(vote => vote.isPositive).length;
  const negative = votes.length - positive;
  return { positive, negative };
};

const divideByAnswerId = (votes, answerId) => divideVotes(votesByAnswerId(votes, answerId));

const answerSortByVote= (answerSort , answers) =>{
    switch (answerSort){
        case 'best': answers.sort((a , b) => a.like - b.like).reverse() ;break;
        case 'worst': answers.sort((a , b) => a.dislike - b.dislike).reverse() ;break;
        default:

    }
};

const enhance = compose(
  connect(mapStateToProps),
  withStateHandlers({ answers: [], users: [], votes: [], isFetching: true }),

  withRouter,

  lifecycle({
    componentWillMount() {
      this.interval = db.pooling(async () => {
        const questionId = this.props.match.params.questionId;

        let answers = await db.answers.find();
        answers = answers.filter(answer => answer.questionId === questionId);

        let votes = await db.votes.find();
        const answerIds = answers.map(a => a._id);
        votes = votes.filter(vote => answerIds.includes(vote.answerId));

        //inject Like, Dislike to answer
        answers.map((answer) =>{
            const { positive, negative } = divideByAnswerId(votes, answer._id);
            answer.like = positive;
            answer.dislike = negative;
            return false;
        });

        //Get answerSort value from store
        const answerSort = store.getState().answerSort;

        answerSortByVote(answerSort, answers);


        const users = await db.users.find();

        this.setState({ answers, users, isFetching: false });
      });
    },
    componentWillUnmount() {
      clearInterval(this.interval);
    }
  }),

  branch(
    ({ isFetching }) => isFetching,
    renderComponent(AppLoader)
  ),

  withHandlers({
    onVote: ({ user }) => (answerId, isPositive) => {
      if (user) {
        db.votes.insert({
          answerId,
          isPositive,
          createdAt: new Date(),
          createdById: user._id,
        });
      }
    }
  }),
);


export default enhance(Component);
