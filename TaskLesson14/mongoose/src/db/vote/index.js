const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const VoteSchema = Schema({
    isPositive: Boolean,
    createdAt: Date,
    ansverId: { type: Schema.Types.ObjectId, ref: "ansver" },
    createdById: { type: Schema.Types.ObjectId, ref: "User" }
});

module.exports = mongoose.model("Vote", VoteSchema);