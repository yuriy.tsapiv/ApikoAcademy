const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AnswerSchema = Schema({
    title: String,
    description: String,
    questionId: { type: Schema.Types.ObjectId, ref: "Question" },
    createdAt: Date,
    createdById: { type: Schema.Types.ObjectId, ref: 'User' }
});

module.exports = mongoose.model("Answer", AnswerSchema);