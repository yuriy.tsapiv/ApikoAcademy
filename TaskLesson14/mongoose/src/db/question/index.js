const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const QuestionSchema = Schema({
    title: String,
    description: String,
    tags: [String],
    createdAt: Date,
    createdById: { type: Schema.Types.ObjectId, ref: 'User' }
});

module.exports = mongoose.model("Question", QuestionSchema);