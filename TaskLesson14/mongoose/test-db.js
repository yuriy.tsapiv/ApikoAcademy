const mongoose = require('mongoose');
const User = require('./src/db/user');
const Question = require('./src/db/question');
const Answer = require('./src/db/answer');
const Vote = require('./src/db/vote');

//add collections
User.create();
Question.create();
Answer.create();
Vote.create();

//add users
const user1 = new User({
    email: "user1@gmail.com",
    profile: {
        fullNmae: "User 1",
        post: "Some post User 1"
    },
    servise: "Some servise User 1"
});
user1.save((err, user) => !err ? console.log(`User1 : ${user}`) : console.log(err));

const user2 = new User({
    email: "user2@gmail.com",
    profile: {
        fullNmae: "User 2",
        post: "Some post User 2"
    },
    servise: "Some servise User 2"
});
user2.save((err, user) => !err ? console.log(`User2 : ${user}`) : console.log(err));

const user3 = new User({
    email: "user3@gmail.com",
    profile: {
        fullNmae: "User 3",
        post: "Some post User 3"
    },
    servise: "Some servise User 3"
});
user3.save((err, user) => !err ? console.log(`User3 : ${user}`) : console.log(err));

//add question User 1
const qustionUser1 = new Question({
    title: 'Title qustionUser1',
    description: 'description qustionUser1',
    tags: ['String'],
    createdAt: new Date(),
    createdById: user1._id
});
qustionUser1.save((err, question) => !err ? console.log(`Question User1 : ${question}`) : console.log(err));

//add answers User2 for question User 1
const anwerUser2_1 = new Answer({
    title: "Title anwerUser2_1",
    description: "description anwerUser2_1",
    questionId: qustionUser1._id,
    createdAt: new Date(),
    createdById: user2._id
});
anwerUser2_1.save((err, anwer) => !err ? console.log(`Answer1 User2 : ${anwer}`) : console.log(err));

const anwerUser2_2 = new Answer({
    title: "Title anwerUser2_2",
    description: "description anwerUser2_2",
    questionId: qustionUser1._id,
    createdAt: new Date(),
    createdById: user2._id
});
anwerUser2_2.save((err, anwer) => !err ? console.log(`Answer2 User2 : ${anwer}`) : console.log(err));

//add vote User 3 for ansvers User 2
const voteUser3_1 = new Vote({
    isPositive: true,
    createdAt: new Date(),
    ansverId: anwerUser2_1._id,
    createdById: user3._id
});
voteUser3_1.save((err, vote) => !err ? console.log(`Vote1 User3 for anwerUser2_1 : ${vote}`) : console.log(err));

const voteUser3_2 = new Vote({
    isPositive: false,
    createdAt: new Date(),
    ansverId: anwerUser2_2._id,
    createdById: user3._id
});
voteUser3_2.save((err, vote) => !err ? console.log(`Vote2 User3 for anwerUser2_2 : ${vote}`) : console.log(err));