const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

const schema = Schema({
    isPositive: {
        type: Boolean,
        required: true
    },
    createdAt: {
        type: Date,
        required: true
    },
    answerId: {
        type: ObjectId,
        ref: "Ansver",
        required: true
    },
    createdById: {
        type: ObjectId,
        ref: "User",
        required: true
    }
});

module.exports = { schema };