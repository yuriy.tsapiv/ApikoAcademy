const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;


const schema = Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    questionId: {
        type: ObjectId,
        ref: "Question",
        required: true
    },
    createdAt: {
        type: Date,
        required: true,
    },
    createdById: {
        type: ObjectId,
        ref: 'User',
        required: true
    }
});

module.exports = { schema };