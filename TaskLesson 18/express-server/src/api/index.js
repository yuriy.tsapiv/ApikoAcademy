const express = require('express');

const { errorHandler } = require('../middleware');
// list of models here
const { User } = require('../models/user');
const { Question } = require('../models/question');
const { Answer } = require('../models/answer');
const { Vote } = require('../models/vote');

// list of controllers here
const users = require('../controllers/users');
const questions = require('../controllers/questions');
const answers = require('../controllers/anwers');
const votes = require('../controllers/votes');

// combine models ino one object
const models = { User, Question, Answer, Vote };

const routersInit = config => {
  const router = express();

  // register api points
  router.use('/users', users(models, { config }));
  router.use('/questions', questions(models, { config }));
  router.use('/answers', answers(models, { config }));
  router.use('/votes', votes(models, { config }));


  // catch api all errors
  router.use(errorHandler);
  return router;
};

module.exports = routersInit;