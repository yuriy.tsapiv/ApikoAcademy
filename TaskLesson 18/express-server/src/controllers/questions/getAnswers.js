const getAnswers = ({ Answer }, { config }) => async (req, res, next) => {
  const { _id } = req.params;
  const questionId = _id;
  
  try {
    const answers = await Answer.find({ questionId });
    res.status(200).send({ answers });
  } catch (error) {
    next(error);
  }
};

module.exports= { getAnswers };
