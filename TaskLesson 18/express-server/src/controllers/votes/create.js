const { NotAcceptable } = require('rest-api-errors');
const { sendOne , sendCreate } = require('../../middleware');
const _ = require('lodash');

const create = ({ Vote }, { config }) => async (req, res, next) => {
  try {
    const vote = new Vote();

    if (!vote.createdAt) {
      vote.createdAt = new Date();
    }

    _.extend(vote, req.body);
    await vote.save();

    return sendOne(res, { vote });
  } catch (error) {
    next(error);
  }
};

module.exports= { create };
