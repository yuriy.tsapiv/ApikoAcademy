const getVotes = ({ Vote }, { config }) => async (req, res, next) => {
    const { _id } = req.params;
    const answerId = _id;
    
    try {
      const votes = await Vote.find({ answerId });
      res.status(200).send({ votes });
    } catch (error) {
      next(error);
    }
  };
  
  module.exports= { getVotes };
  