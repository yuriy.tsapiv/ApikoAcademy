const { NotAcceptable } = require('rest-api-errors');
const { sendOne , sendCreate } = require('../../middleware');
const _ = require('lodash');

const create = ({ Answer }, { config }) => async (req, res, next) => {
  try {
    const answer = new Answer();

    if (!answer.createdAt) {
      answer.createdAt = new Date;
    };

    _.extend(answer, req.body);
    await answer.save();

    return sendOne(res, { answer });
  } catch (error) {
    next(error);
  };
};

module.exports= { create };
