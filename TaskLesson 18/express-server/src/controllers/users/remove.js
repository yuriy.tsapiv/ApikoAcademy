const _ = require('lodash');

const remove = ({ User }, { config }) => async (req, res, next) => {
  const { _id } = req.params;
  try {
    const user = await User.findOne({ _id });
    await User.remove({ _id });
    res.status(200).send({ user });
  } catch (error) {
    next(error);
  }
};

module.exports= { remove };
