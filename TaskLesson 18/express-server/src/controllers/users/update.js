const _ = require('lodash');

const update = ({ User }, { config }) => async (req, res, next) => {
  const { _id } = req.params;
  try {
    const user = await User.findOne({ _id });
    _.extend(user, req.body);
    await user.save();
    res.status(200).send({ user });
  } catch (error) {
    next(error);
  }
};

module.exports= { update };
