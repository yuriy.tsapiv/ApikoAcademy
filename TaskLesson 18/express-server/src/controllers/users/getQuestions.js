const getQuestions = ({ Question }, { config }) => async (req, res, next) => {
    const { _id } = req.params;
    const createdById = _id;
    
    try {
      const questions = await Question.find({ createdById });
      res.status(200).send({ questions });
    } catch (error) {
      next(error);
    }
  };
  
  module.exports= { getQuestions };
  