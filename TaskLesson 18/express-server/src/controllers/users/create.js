const { NotAcceptable } = require('rest-api-errors');
const { sendOne , sendCreate } = require('../../middleware');
const _ = require('lodash');

const create = ({ User }, { config }) => async (req, res, next) => {
  try {
    const user = new User();

    _.extend(user, req.body);
    await user.save();

    return sendOne(res, { user });
  } catch (error) {
    next(error);
  }
};

module.exports= { create };
