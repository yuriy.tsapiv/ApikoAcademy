const API = 'https://jsonplaceholder.typicode.com/'; //Change URL to get error connect. 

const contentDiv = document.getElementById("content");

const errorReuest = `<h1 class="error">The information for this request does not exist!</h1>`

async function fetchData(urlApi) {
    try {
        const response = await fetch(urlApi);
        const data = await response.json();
        return data;
    } catch (error) {
        contentDiv.innerHTML = (`<h1 class="error">Error connect to service !!!</h1>`);
    };
}

let getUrlApi = () => {
    let fragmentHash = location.hash.substr(1);
    return `${API}${fragmentHash}`;
};


let switchRoute = () => {
    const locHath = location.hash;
    
    if (locHath.search("userId") > 0){
        postsUser()
    }else if (locHath.search("postId") > 0){
        commentsPost()
    }else if(locHath.search("posts") > 0){
        postsList()
    }else{
        render(errorReuest).then( document.location.reload());
    }
   
};

let render = (markup) => {
    contentDiv.innerHTML = markup;
    window.scrollTo(0, 0);
}

let postsList = () => {
    fetchData(getUrlApi()).then(data => {
        const markup = `
            <div class="navbar navbar-inverse  navbar-fixed-top container text-center">
                <h1 class="head_page">Posts</h1>
            </div>    
            ${data.map(data => `
            <div class="panel panel-info">
            <div class="panel-heading ">
                <h3>
                    ${data.title}
                </h3>
            </div>
            <div class="panel-body">
                <p>${data.body}</p>
            </div>
            <div class="panel-footer">
                <a href="#posts?userId=${data.userId}">
                    <img class="user-small-img" src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png" />
                     User ${data.userId}
                </a>
                <a href="#comments?postId=${data.id}" class="pull-right">Comments post ${data.id}  </a>
            </div>
        </div>`)}   
        `;
        render(markup);
    }).catch(error => {error = render(errorReuest)});
}

let commentsPost = () => {
    fetchData(getUrlApi()).then(data => {
        const markup = `
        <div class="navbar navbar-inverse  navbar-fixed-top container text-center">
            <div onclick="goBack()" class="goBack btn">< Back</div>
            <h1 class="head_page">Comments post ${data[0].postId}</h1>
         </div>  
        ${data.map(data => `
        <div class="panel panel-warning">
            <div class="panel-heading ">
                <h3>
                    <p>${data.name}</p>
                </h3>
            </div>
            <div class="panel-body">
                <p>${data.body}</p>
            </div>
            <div class="panel-footer">
                Comment ${data.id}
                <a class="pull-right" href="mailto:${data.email}">${data.email}</a>
            </div>
        </div>`)}      
        `;
        render(markup);
    }).catch(error => {error = render(errorReuest)});
}

let postsUser = () => {
    fetchData(getUrlApi()).then(data => {
       const markup = `
        <div class="navbar navbar-inverse  navbar-fixed-top container text-center">
            <div onclick="goBack()"  class="goBack btn">< Back</div>
            <h1 class="head_page">Posts User ${data[0].userId} </h1>
         </div> 
        
        ${data.map(data => `
        <div class="panel panel-success">
            <div class="panel-heading ">
                <h3>
                    ${data.title}
                </h3>
            </div>
            <div class="panel-body">
                <p>${data.body}</p>
            </div>
            <div class="panel-footer">
                <a href="#comments?postId=${data.id}" class="">Comments post ${data.id}  </a>
            </div>
        </div>`)}
        `;
        render(markup);
    }).catch(error => {error = render(errorReuest)});
}

let goBack =() =>  window.history.back();

//navigation
let navigate = () => {switchRoute()};
 
(!location.hash) ? location.hash = '#posts' : '';

navigate();

window.addEventListener("hashchange", navigate);