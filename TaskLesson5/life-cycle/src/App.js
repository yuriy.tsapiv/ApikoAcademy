import React, { Component } from 'react';
import './App.css';
import PostList from './Components/Posts/PostList.js';
import MoreButton from './Components/Buttons/MoreButton.js';
import Header from './Components/Header/Header.js';
import Loading from './Components/Loading/Loading';

const API = "https://jsonplaceholder.typicode.com/";

const fetchData = (entity) => fetch(API + entity)
  .then(response => response.json())
  .catch(error => document.write(error));

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      limit: 10,
      search: '',
      posts: [],
      isLoading: true
    }

    this.loadMore = this.loadMore.bind(this);
    this.searchPost = this.searchPost.bind(this);

  }

  searchPost({ target: { value } }) {
    this.setState({
      search: value,
      limit: 10 //set limit to default value
    });
  }

  loadMore() {
    this.setState({
      limit: this.state.limit + 10
    });
  }

  componentDidMount() {
    console.log("componentDidMount app");
    this.pulingFetch = setInterval(() => {
      console.log("pulingFetch to api");
      Promise.all([fetchData("posts")]).then(
        ([posts]) => {
          this.setState({
            posts,
            isLoading: false
          });
        }
      );
    }, 5000);
  }

  componentWillUnmount() {
    console.log("clear interval app");
    clearInterval(this.pulingFetch);
  }


  render() {
    let { limit, search, posts, isLoading } = this.state;
    let filteredPosts = posts.filter(
      (post) => {
        return post.title.toLocaleLowerCase().indexOf(search.toLocaleLowerCase()) !== -1;
      }
    );

    if (isLoading) {
      return (
        <Loading />
      )
    }

    return (
      <React.Fragment>
        <Header shownPosts={limit}
          foundPost={filteredPosts.length}
          searchPost={this.searchPost} />
        <PostList data={filteredPosts} limit={limit} />
        {limit < filteredPosts.length ? <MoreButton loadMore={this.loadMore} /> : ''}
      </React.Fragment>
    );
  }


}

export default App;
