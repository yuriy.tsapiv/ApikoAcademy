import React, { Component } from 'react'
import './PostListItem.css'

export default class PostListItem extends Component {
  
  shouldComponentUpdate(nextProps) {
    let { title, body, userId, id } = this.props;
    let result = title !== nextProps.title ||
    body !== nextProps.body ||
    userId !== nextProps.userId ||
    id !== nextProps.id;
    console.log("shouldComponentUpdate, render Item", result);
    return result;
  }

  render() {
    let { title, body, userId, id } = this.props;
    console.log("render Item");

    return (
      <li className="PostListItem">
        <div className="panel panel-info">
          <div className="panel-heading">
            <h3>{title}</h3>
          </div>
          <div className="panel-body">
            {body}
          </div>
          <div className="panel-footer">
            <span>Post id {id}</span>
            <span className="pull-right">User id {userId}</span>
          </div>
        </div>
      </li>
    )
  }
}
