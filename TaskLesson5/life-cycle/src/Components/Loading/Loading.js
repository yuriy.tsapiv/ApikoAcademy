import React, { Component } from 'react';
import load from '../../Double Ring-1s-200px.gif';
import './Loading.css';

export default class Loading extends Component {
  render() {
    return (
      <div className="Loading text-center" >
        <img src={load} width="200" height="200" alt="image deleted"/>
        <h1>Loading...</h1>
      </div>
    )
  }
}
