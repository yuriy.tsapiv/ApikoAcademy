import React, { Component } from 'react';
import './Search.css';

export default class Search extends Component {
  render() {
    let {searchPost, foundPost } = this.props;
    return (
      <div className="Search">
        <input className="form-control col-xs-4" placeholder="Search post ...."
          onChange={searchPost} />
        <h4 className="col-xs-4">
          {foundPost !== 0 ?
            `Found ${foundPost} post(s)` :
            `No items found`}
        </h4>
      </div>
    )
  }
}
