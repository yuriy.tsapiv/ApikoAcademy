import React, { Component } from 'react'
import './Header.css'
import Search from './../Inputs/Search';

export default class Header extends Component {
  render() {
    let {shownPosts, foundPost, searchPost} = this.props
    return (
      <div className="Header navbar navbar-inverse navbar-fixed-top text-center container">
        <Search foundPost={foundPost} searchPost={searchPost} />
        <h4>Shown {shownPosts > foundPost ? foundPost: shownPosts} posts</h4>
      </div>
    )
  }
}
