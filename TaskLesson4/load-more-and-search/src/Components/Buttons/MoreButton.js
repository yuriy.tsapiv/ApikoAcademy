import React, { Component } from 'react'
import './MoreButton.css'

export default class MoreButton extends Component {
  render() {
    return (
        <button onClick={this.props.loadMore} className="MoreButton btn btn-success btn-block">
           Show 10 more posts
        </button>
    )
  }
}
