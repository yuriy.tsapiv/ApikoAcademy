import React, { Component } from 'react'
import './PostListItem.css'

export default class PostListItem extends Component {
  
  render() {
    let{ title, body,userId,id} = this.props
    return (
      <li className="PostListItem">
        <div className="panel panel-info">
          <div className="panel-heading">
            <h3>{title}</h3>
          </div>
          <div className="panel-body">{body}</div>
          <div className="panel-footer">
            <span>Post id {id}</span>
            <span className="pull-right">User id {userId}</span>
          </div>
        </div>
      </li>
    )
  }
}
