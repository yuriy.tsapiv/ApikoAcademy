import React, { Component } from 'react';
import './App.css';
import data from './data.js';
import PostList from './Components/Posts/PostList.js';
import MoreButton from './Components/Buttons/MoreButton.js';
import Header from './Components/Header/Header.js';

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      limit: 10,
      search: ''
    }

    this.loadMore = this.loadMore.bind(this);
    this.searchPost = this.searchPost.bind(this);
  }

  searchPost({ target: { value } }) {
    this.setState({
      search: value,
      limit: 10
    });
  }

  loadMore() {
      this.setState({
        limit: this.state.limit + 10
      });
  }

  render() {
    let { limit, search } = this.state;
    let filteredPosts = data.filter(
      (post) => {
        return post.title.toLocaleLowerCase().indexOf(search.toLocaleLowerCase()) !== -1;
      }
    );
    return (
      <React.Fragment>
        <Header
          shownPosts={limit}
          foundPost={filteredPosts.length}
          searchPost={this.searchPost} />
        <PostList data={filteredPosts} limit={limit} />
        {limit < filteredPosts.length ? <MoreButton loadMore={this.loadMore} /> : ''}
      </React.Fragment>
    );
  }
}

export default App;
