import React, { Component } from 'react';
import './App.css';
import data from './data.js';
import PostList from './Components/Posts/PostList.js';
import MoreButton from './Components/Buttons/MoreButton.js';
import Header from './Components/Header/Header.js';

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      limit: 10
    }
    this.loadMore = this.loadMore.bind(this);
  }

  loadMore() {
      this.setState({
        limit: this.state.limit + 10
      });
  }

  render() {
    let { limit } = this.state;
    return (
      <React.Fragment>
        <Header numPosts={limit} />
        <PostList data={data} limit={limit} />
        {limit < data.length ? <MoreButton loadMore={this.loadMore} /> : ''}
      </React.Fragment>
    );
  }
}

export default App;
