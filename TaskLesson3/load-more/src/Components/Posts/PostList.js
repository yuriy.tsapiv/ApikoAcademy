import React, { Component } from 'react'
import PostListItem from './PostListItem';

export default class PostList extends Component {
  constructor(props) {
    super(props)

    this.renderPostsListItem = this.renderPostsListItem.bind(this);
  }

  renderPostsListItem() {
    let { limit, data } = this.props;
    return data.slice(0, limit).map((post) =>
      <PostListItem key={post.id} {...post} />
    )
  }

  render() {
    return (
      <ul className="PostList">
        {this.renderPostsListItem()}
      </ul>
    )
  }
}

