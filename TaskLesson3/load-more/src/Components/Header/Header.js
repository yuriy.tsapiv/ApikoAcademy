import React, { Component } from 'react'
import './Header.css'

export default class Header extends Component {
  render() {
    return (
      <div className="Header navbar navbar-inverse navbar-fixed-top text-center container">
        <h3>Shown {this.props.numPosts} posts</h3>
      </div>
    )
  }
}
