import React, { Component } from 'react'
import './MoreButton.css'

export default class MoreButton extends Component {
  render() {
    return (
      <div className="MoreButton">
        <button onClick={this.props.loadMore} className="btn btn-success btn-block">
        Show 10 more posts
        </button>
      </div>
    )
  }
}
