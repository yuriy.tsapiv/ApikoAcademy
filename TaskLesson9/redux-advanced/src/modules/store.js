import { createStore, applyMiddleware } from 'redux';
// TODO: HOMEWORK 9: install and import thunk middleware, DONE
import thunk from 'redux-thunk';
import {createLogger} from 'redux-logger';
import { autoRehydrate, persistStore } from 'redux-persist';
import { composeWithDevTools } from 'redux-devtools-extension';

import reducer from './';

// TODO: HOMEWORK 9: apply thunk middleware, DONE
const middleware = [thunk];
middleware.push(createLogger());

const store = createStore(reducer, undefined, composeWithDevTools(applyMiddleware(...middleware), autoRehydrate()));

persistStore(store, { whitelist: ['user'] });

export default store;
