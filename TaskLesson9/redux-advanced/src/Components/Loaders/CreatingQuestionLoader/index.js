import React from 'react';
import loadImg from './loader-creation-question.gif';

const CreatingQuestionLoader = () => (
    <div style={{textAlign : "center"}}>
        <img width={200} height={200}
             src={loadImg}
             alt="Creating a question ..."
        />
        <h1 style={{color : "blue"}}>Creating a question ...</h1>
    </div>
);

export default CreatingQuestionLoader;